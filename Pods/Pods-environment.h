
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 1
#define COCOAPODS_VERSION_MINOR_AFNetworking 3
#define COCOAPODS_VERSION_PATCH_AFNetworking 2

// DCIntrospect
#define COCOAPODS_POD_AVAILABLE_DCIntrospect
#define COCOAPODS_VERSION_MAJOR_DCIntrospect 0
#define COCOAPODS_VERSION_MINOR_DCIntrospect 0
#define COCOAPODS_VERSION_PATCH_DCIntrospect 2

// pop
#define COCOAPODS_POD_AVAILABLE_pop
#define COCOAPODS_VERSION_MAJOR_pop 1
#define COCOAPODS_VERSION_MINOR_pop 0
#define COCOAPODS_VERSION_PATCH_pop 4

