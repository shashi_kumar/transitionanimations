//
//  FriendListsCell.h
//  TransitionAnimations
//
//  Created by shashi kumar on 5/16/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FriendListsCell;

@protocol HCSFriendListsCellDelegate <NSObject>

- (void)show;

@end

@interface FriendListsCell : UITableViewCell
+ (NSString *)reuseIdentifier;
- (void)configureCellWithName:(NSString *)userName dpImage:(UIImage *)dpImage rank:(NSInteger)rank isOnline:(BOOL)isOnline;

@property (nonatomic, weak) id<HCSFriendListsCellDelegate> delegate;
@end
