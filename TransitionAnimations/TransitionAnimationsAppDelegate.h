//
//  TransitionAnimationsAppDelegate.h
//  TransitionAnimations
//
//  Created by shashi kumar on 5/15/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransitionAnimationsAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
