//
//  TransitionAnimationsViewController.m
//  TransitionAnimations
//
//  Created by shashi kumar on 5/15/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import "TransitionAnimationsViewController.h"
#import "FriendListsViewController.h"

@interface TransitionAnimationsViewController ()
@property (nonatomic, strong) FriendListsViewController *friendListViewController;
@property (nonatomic, strong) UINavigationController *navController;
@end

@implementation TransitionAnimationsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.navController  = [[UINavigationController alloc] init];
    [self.view addSubview:self.navController.view];
    [self setupButton];
	// Do any additional setup after loading the view, typically from a nib.
}

- (FriendListsViewController *)friendListViewController {
    if (!_friendListViewController) {
        _friendListViewController = [[FriendListsViewController alloc] init];
    }
    return _friendListViewController;
}

- (void)setupButton {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(30, 80, 100, 30)];
    [button setTitle:NSLocalizedString(@"button", nil) forState:UIControlStateNormal];
    [button addTarget:self action:@selector(pushButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:button];
    
}

- (void)pushButtonDidTap:(UIButton *)sender {
    
    [self.navController pushViewController:self.friendListViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
