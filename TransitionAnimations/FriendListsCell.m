//
//  FriendListsCell.m
//  TransitionAnimations
//
//  Created by shashi kumar on 5/16/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import "FriendListsCell.h"

@interface FriendListsCell ()
@property (nonatomic, strong) UIImageView *dpImageView;
@property (nonatomic, strong) UIImageView *onlineImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *rankLabel;
@property (nonatomic, strong) UIButton *button;
@end

@implementation FriendListsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _dpImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 40, 40)];
        [_dpImageView.layer setMasksToBounds:YES];
        [_dpImageView.layer setCornerRadius:12];
        [_dpImageView.layer setBorderColor:[[UIColor blackColor] CGColor]];
        [_dpImageView.layer setBorderWidth:1.0f/[[UIScreen mainScreen] scale]];
        [self.contentView addSubview:_dpImageView];
        
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button setFrame:CGRectMake(self.frame.size.width - 100, 2, 98, 40)];
        [_button setBackgroundColor:[UIColor blueColor]];
        [_button.layer setMasksToBounds:YES];
        [_button.layer setCornerRadius:10.0f];
        [_button setTitle:NSLocalizedString(@"challange", nil) forState:UIControlStateNormal];
        [_button addTarget:self action:@selector(buttonDidTap:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_button];
        
        _onlineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_button.frame.origin.x - 10, 19, 6, 6)];
        [_onlineImageView.layer setMasksToBounds:YES];
        [_onlineImageView.layer setCornerRadius:2];
        [_onlineImageView.layer setBorderColor:[[UIColor grayColor] CGColor]];
        [_onlineImageView.layer setBorderWidth:1.0f/[[UIScreen mainScreen] scale]];
        [self.contentView addSubview:_onlineImageView];
        
        CGFloat nameLabelX = _dpImageView.frame.origin.x + _dpImageView.frame.size.width + 5;
        CGFloat nameLabelWidth = self.frame.size.width - (_dpImageView.frame.origin.x + _dpImageView.frame.size.width) - _button.frame.size.width - _onlineImageView.frame.size.width - 6;
        
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(nameLabelX, 2, nameLabelWidth , 20)];
        [_nameLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:_nameLabel];
        
        _rankLabel = [[UILabel alloc] initWithFrame:CGRectMake(nameLabelX, 22, nameLabelWidth , 20)];
        [_rankLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:_rankLabel];

    }
    return self;
}

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([FriendListsCell class]);
}

- (void)configureCellWithName:(NSString *)userName dpImage:(UIImage *)dpImage rank:(NSInteger)rank isOnline:(BOOL)isOnline {
    [_nameLabel setText:userName];
    [_rankLabel setText:[NSString stringWithFormat:@"%ld",(long)rank]];
    [_dpImageView setImage:dpImage];
    if (isOnline) {
        [_onlineImageView setImage:[UIImage imageNamed:@""]];
    } else {
        [_onlineImageView setImage:[UIImage imageNamed:@""]];
    }
    
}

- (void)buttonDidTap:(UIButton *)sender {
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
