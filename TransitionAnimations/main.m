//
//  main.m
//  TransitionAnimations
//
//  Created by shashi kumar on 5/15/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TransitionAnimationsAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TransitionAnimationsAppDelegate class]));
    }
}
