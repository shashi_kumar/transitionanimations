//
//  FriendListsViewController.m
//  TransitionAnimations
//
//  Created by shashi kumar on 5/16/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import "FriendListsViewController.h"
#import "FriendListsCell.h"

static CGFloat const settingViewButtonHeight = 30.0f;

@interface FriendListsViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *friendListView;
@property (nonatomic, strong) UIView *settingsButtonView;
@end

@implementation FriendListsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.settingsButtonView];
    [self.view addSubview:self.friendListView];

	// Do any additional setup after loading the view.
}

- (UIView *)settingsButtonView {
    if (!_settingsButtonView) {
        _settingsButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, settingViewButtonHeight)];
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(self.view.frame.size.width - 100, 2, 98, 26)];
        [button setBackgroundColor:[UIColor blueColor]];
        [button.layer setMasksToBounds:YES];
        [button.layer setCornerRadius:10.0f];
        [button setTitle:NSLocalizedString(@"settings", nil) forState:UIControlStateNormal];
        [button addTarget:self action:@selector(settingsButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
        [_settingsButtonView addSubview:button];
        
        [_settingsButtonView setBackgroundColor:[UIColor redColor]];
    }
    return _settingsButtonView;
}

- (UITableView *)friendListView {
    if (!_friendListView) {
        _friendListView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.settingsButtonView.frame.origin.y + settingViewButtonHeight + 2, self.view.frame.size.width, self.view.frame.size.height - 30) style:UITableViewStylePlain];
        _friendListView.delegate = self;
        _friendListView.dataSource = self;
        _friendListView.separatorColor = [UIColor grayColor];
        [_friendListView registerClass:[FriendListsCell class] forCellReuseIdentifier:[FriendListsCell reuseIdentifier]];
    }
    return _friendListView;
}

#pragma mark - actions

- (void)settingsButtonDidTap:(UIButton *)sender {
    
}

#pragma mark - tableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 4;
    }
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendListsCell *cell = [tableView dequeueReusableCellWithIdentifier:[FriendListsCell reuseIdentifier] forIndexPath:indexPath];
    if (!cell) {
        cell = [[FriendListsCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell configureCellWithName:@"shashi" dpImage:nil rank:1 isOnline:YES];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [view setBackgroundColor:[UIColor grayColor]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(2, 0, view.frame.size.width - 2, view.frame.size.height)];
    if (section == 0) {
        label.text = NSLocalizedString(@"online friends", nil);
    } else {
        label.text = NSLocalizedString(@"offline friends", nil);
    }
    label.backgroundColor = [UIColor clearColor];
    [view addSubview:label];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //1. Setup the CATransform3D structure
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0/ -600;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor grayColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    
    //3. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
